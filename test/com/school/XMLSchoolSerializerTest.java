package com.school;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;

public class XMLSchoolSerializerTest {

	private XMLSchoolSerializer serializer;

	@Before
	public void setUp() throws Exception {
		serializer = new XMLSchoolSerializer();
	}

	@Test
	public void testBuildFromFile() throws IOException {
		School school = serializer.buildFromFile("test/data/sample_data.xml");
		assertNotNull(school);
		
		assertEquals(school.grades.size(), 1);
	}

	@Test
	public void testWriteToFile() throws IOException {
		School school = serializer.buildFromFile("test/data/sample_data.xml");
		Document doc = serializer.buildXML(school); 
		String out = XMLSchoolSerializer.domToString(doc);
		String expected = FileUtils.readFileToString(new File("test/data/output.xml"));
		assertEquals(expected.replaceAll("\n|\r", ""), out.replaceAll("\n|\r", ""));
	}
}
