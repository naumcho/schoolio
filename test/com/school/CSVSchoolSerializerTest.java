package com.school;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;


public class CSVSchoolSerializerTest {

	private CSVSchoolSerializer serializer;

	@Before
	public void setUp() throws Exception {
		 serializer = new CSVSchoolSerializer();
	}

	@Test
	public void testBuildFromFile() throws IOException {
		School school = serializer.buildFromFile("test/data/sample_data.csv");
		assertNotNull(school);
		
		assertEquals(school.grades.size(), 1);
	}

	@Test
	public void testWriteToFile() throws IOException {
		School school = serializer.buildFromFile("test/data/sample_data.csv");
		
		List<String> output = serializer.buildOutput(school);
		String out = serializer.saveToString(output);
		String expected = FileUtils.readFileToString(new File("test/data/output.csv"));
		assertEquals(expected.replaceAll("\n|\r", ""), out.replaceAll("\n|\r", ""));
	}
}
