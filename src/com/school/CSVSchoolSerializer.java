package com.school;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * CSV in/out serializer
 * 
 * @author root
 *
 */
public class CSVSchoolSerializer extends SchoolSerializer {

	final String[] headers = {"classroom id", "classroom_name", "teacher_1_id", "teacher_1_last_name", "teacher_1_first_name", "teacher_2_id", "teacher_2_last_name", "teacher_2_first_name", "student_id", "student_last_name", "student_first_name", "student_grade"};
	
	/* (non-Javadoc)
	 * @see com.school.SchoolSerializer#buildFromFile(java.lang.String)
	 */
	@Override
	public School buildFromFile(String inFileName) throws IOException {
		
		List<Map<String, String>> rows = readCSVFile(inFileName);
		
		School school = new School();
		ResourceStore resourceStore = new ResourceStore();

		for (Map<String, String> map : rows) {
			
			//Find reference or make new objects
			String gradeId = map.get("student_grade");
			String studentId = map.get("student_id");
			String teacher1Id = map.get("teacher_1_id");
			String teacher2Id = map.get("teacher_2_id");

			
			gradeId = sanitizeGradeId(gradeId, studentId, teacher2Id);
			
			Grade grade = resourceStore.grades.get(gradeId);
			if (grade == null) {
				grade = new Grade(gradeId);
				resourceStore.grades.put(gradeId, grade);
			}
			school.addGrade(gradeId,grade);
			
			String roomId = map.get("classroom id");
			Classroom room = resourceStore.classrooms.get(roomId);
			if (room == null) {
				String roomName = map.get("classroom_name");
				room = new Classroom(roomId, roomName);
				resourceStore.classrooms.put(roomId, room);
			}
			grade.addClassroom(roomId, room);

		
			if (teacher1Id != null && !teacher1Id.isEmpty()) {
				Teacher teacher1 = resourceStore.teachers.get(teacher1Id);
				if (teacher1 == null) {
					String firstName = map.get("teacher_1_first_name");
					String lastName = map.get("teacher_1_last_name");
					teacher1 = new Teacher(teacher1Id, firstName, lastName);
					resourceStore.teachers.put(teacher1Id, teacher1);
				}
				//also add a reference to the grade-classroom
				room.addTeacher(teacher1Id, teacher1);
			}

			
			//it looks like the second teacher is optional
			if (teacher2Id != null && !teacher2Id.isEmpty()) {
				Teacher teacher2 = resourceStore.teachers.get(teacher2Id);
				if (teacher2 == null) {
					String firstName = map.get("teacher_2_first_name");
					String lastName = map.get("teacher_2_last_name");
					teacher2 = new Teacher(teacher2Id, firstName, lastName);
					resourceStore.teachers.put(teacher2Id, teacher2);
				}
				//also add a reference to the grade-classroom
				room.addTeacher(teacher2Id, teacher2);
			}
			
			//It looks like the student section is optional and you can hae rooms with no students
			if (studentId != null && !studentId.isEmpty()) {
				Student student = resourceStore.students.get(studentId);
				if (student == null) {
					String firstName = map.get("student_first_name");
					String lastName = map.get("student_last_name");
					student = new Student(studentId, firstName, lastName);
					resourceStore.students.put(studentId, student);
				}
				//also add a reference to the grade-classroom
				room.addStudent(studentId, student);
			}

		}
		
		return school;
	}
	
	
	/**
	 * This function catches errors with grades and corrects them when possible
	 * 
	 * @param gradeId
	 * @param studentId
	 * @param teacher2Id 
	 * @return
	 */
	private String sanitizeGradeId(String gradeId, String studentId, String teacher2Id) {
		//default grade to 1 since that's what it appears to be in the example (or is it taking the last used one?)
		String newGradeId = gradeId;
		
		//somties the grade might be missing
		if (newGradeId == null || newGradeId.isEmpty()) {
			newGradeId = "1";
		}
		
		//in some cases the grade of the student appears to be incorrect so we try to detect and correct those
		//e.g. we can assume that when only 1 teacher is teaching a class it's a grade 1 class
		else if ((teacher2Id == null || teacher2Id.isEmpty()) && !gradeId.equals("1")) {
			newGradeId = "1";
			System.err.println("Mismatching grade " + gradeId + " found for student " + studentId + ", correcting to " + newGradeId);
		}

		return newGradeId;
	}


	/* (non-Javadoc)
	 * @see com.school.SchoolSerializer#writeToFile(com.school.School, java.lang.String)
	 */
	@Override
	public void writeToFile(School school, String path) throws IOException {
		
		List<String> output = buildOutput(school);
		saveFile(path, headers, output);
	}


	/**
	 * @param school
	 * @param headers
	 * @return
	 */
	List<String> buildOutput(School school) {
		List<String> output = Lists.newArrayList();
		
		for(Grade grade : school.grades.values()) {
			for(Classroom room : grade.classrooms.values()) {
				//handle the case where there is only a teacher and no students associated with it
				if (room.students.isEmpty()) {
					Map<String, String> rowmap = populateMapForCSVRow(grade, room, null);
					String row = makeCSVRow(headers, rowmap);
					output.add(row);
				} 
				else for(Student student : room.students.values()) {
					
					Map<String, String> rowmap = populateMapForCSVRow(grade, room, student);
					String row = makeCSVRow(headers, rowmap);
					output.add(row);
				}
			}
		}
		return output;
	}


	/**
	 * @param grade
	 * @param room
	 * @param student
	 * @return
	 */
	private Map<String, String> populateMapForCSVRow(Grade grade, Classroom room,
			Student student) {
		Map<String,String> rowmap = Maps.newHashMap();

		int i = 1; //this format can only handle 2 teachers per row
		for(Teacher teacher : room.teachers.values()) {
			if (i <= 2) {
				rowmap.put("teacher_"+i+"_id", teacher.getId() );
				rowmap.put("teacher_"+i+"_last_name", teacher.getLastName());
				rowmap.put("teacher_"+i+"_first_name", teacher.getFirstName());
			}
			i++;
		}
		//there could be no teacher2-s
		if (room.teachers.values().size() == 1) {
			rowmap.put("teacher_2_id", "" );
			rowmap.put("teacher_2_last_name", "");
			rowmap.put("teacher_2_first_name", "");
		}
		rowmap.put("classroom id", room.getId());
		rowmap.put("classroom_name", room.getName());
		rowmap.put("student_id", student == null ? "" : student.getId());
		rowmap.put("student_last_name", student == null ? "" : student.getLastName());
		rowmap.put("student_first_name", student == null ? "" : student.getFirstName());
		rowmap.put("student_grade", student == null ? "" : grade.getId());
		return rowmap;
	}


	/** Join the headers with the map into a CSV line 
	 * @param headers
	 * @param rowmap
	 * @return
	 */
	private String makeCSVRow(String[] headers,
			Map<String, String> rowmap) {
		//manually do the join()
		StringBuilder sb = new StringBuilder();
		for (int j = 0; j < headers.length; j++) {
			sb.append(rowmap.get(headers[j]));
			if (j != headers.length - 1) sb.append(", ");
		}
		return sb.toString();
	}


	/**
	 * Do the acutal file IO
	 * 
	 * @param path
	 * @param headers
	 * @param output
	 * @throws IOException
	 */
	private void saveFile(String path, String[] headers, List<String> output)
			throws IOException {
		FileWriter fstream = new FileWriter(path);
		BufferedWriter out = new BufferedWriter(fstream);
		try {
			StringBuffer result = new StringBuffer();
			for (int i = 0; i < headers.length; i++) {
			   result.append(headers[i]);
			   result.append(", ");
			}
			out.write(result.toString());
			out.newLine();
			
			for(String s : output) {
				out.write(s);
				out.newLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			//Close the output stream
			out.close();	
		}
	}
	
	/**
	 * Used for unit test only for now
	 * 
	 * @param headers
	 * @param output
	 * @return
	 */
	String saveToString(List<String> output) {
		StringBuffer result = new StringBuffer();
		
		for (int i = 0; i < headers.length; i++) {
		   result.append(headers[i]);
		   if (i != headers.length - 1) result.append(", ");
		}
		result.append("\n");
		
		for(String s : output) {
			result.append(s).append("\n");
		}
		return result.toString();
	}


	/**
	 * Return a list of maps where each row is a map of the column names to the values in the row
	 * This is a simple parser and doesn't handle escaping etc.
	 * 
	 * @param inFileName
	 * @return
	 * @throws IOException
	 */
	private List<Map<String, String>> readCSVFile(String inFileName) throws IOException {
		List<Map<String, String>> res = Lists.newArrayList();
		
		
		String[] headers = null;
		
	    BufferedReader reader = new BufferedReader( new FileReader(inFileName));
	    String line = null;
	    while( ( line = reader.readLine() ) != null ) {
	    	String[] parts = line.split(",");
	    	if (headers == null) {
	    		headers = parts;
	    	} else {
	    		Map<String, String> rowmap = Maps.newHashMap();
	    		for (int i = 0; i < parts.length; i++) {
					rowmap.put(headers[i].trim(), parts[i].trim());
				}
	    		res.add(rowmap);
	    	}
	    		
	    }
		return res;
	}



}
