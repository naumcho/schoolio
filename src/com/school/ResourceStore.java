package com.school;

import java.util.Map;

import com.google.common.collect.Maps;

/**
 * Cache the Resources here after created 
 * @author root
 *
 */
public class ResourceStore {
	//Different resources that the belong to the school
	Map<String, Grade> grades = Maps.newHashMap();
	Map<String, Classroom> classrooms = Maps.newHashMap();
	Map<String, Teacher> teachers = Maps.newHashMap();
	Map<String, Student> students = Maps.newHashMap();
}
