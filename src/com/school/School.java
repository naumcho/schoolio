package com.school;

import java.util.Map;

import com.google.common.collect.Maps;

public class School extends Resource {

	private final static String DEFAULT_SCHOOL_NAME = "WGen School";
	private final static String DEFAULT_SCHOOL_ID = "100";
	
	private final String name;
	
	//Different resources that the belong to the school
	Map<String, Grade> grades = Maps.newLinkedHashMap();


	public School(String id, String name) {
		super(id);
		this.name = name;
	}
	
	public School() {
		super(DEFAULT_SCHOOL_ID);
		this.name = DEFAULT_SCHOOL_NAME;
	}


	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "School [id="+ getId() +", name=" + name + ", grades=" + grades + "]";
	}

	public void addGrade(String gradeId, Grade grade) {
		if (grades.get(gradeId) == null) {
			grades.put(gradeId, grade);
		}
		
	}



}
