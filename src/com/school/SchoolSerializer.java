package com.school;

import java.io.IOException;

/**
 * This is the base class that all serializer type classes need to implement
 * 
 * @author root
 *
 */
public abstract class SchoolSerializer {
	enum FileFormat { CSV, XML };

	/**
	 * Read and parse the file in the appropriate format and return a School object populated with the data
	 * 
	 * @param inFileName
	 * @return
	 * @throws IOException
	 */
	public abstract School buildFromFile(String inFileName) throws IOException;

	/**
	 * Serializer out the data from the school object into a file in location "path"
	 * 
	 * @param school
	 * @param path
	 * @throws IOException
	 */
	public abstract void writeToFile(School school, String path) throws IOException;

	
	/**
	 * This is a facotry method that creates the appropriate serializer for a given file type
	 * 
	 * @param inFileName
	 * @return
	 * @throws IOException
	 */
	public static SchoolSerializer makeSerializer(String inFileName) throws IOException {
		FileFormat type = getType(inFileName);
		SchoolSerializer serializer = null;
		
		switch (type) {
		case CSV:
			serializer = new CSVSchoolSerializer();
			break;
		case XML:
			serializer = new XMLSchoolSerializer();
			break;
		default:
			break;
		}
		
		if (serializer == null) throw new Error("Unknown file format!");
		
		return serializer;
	}
	

	/**
	 * Helper function to determine the file type based on the file name
	 * 
	 * @param inFileName
	 * @return
	 * @throws IOException
	 */
	protected static FileFormat getType(String inFileName) throws IOException {
		String[] parts = inFileName.split("\\.");
		if (parts.length < 2) throw new IOException("Could not get file extension for " + inFileName);
		
		String ext = parts[parts.length-1]; //get last elelent
	
		if (ext.equalsIgnoreCase("xml")) {
			return FileFormat.XML;
		} else if (ext.equalsIgnoreCase("csv")) {
			return FileFormat.CSV;
		} else {
			throw new IOException("Unsupported extension " + ext);
		}		
	}

}