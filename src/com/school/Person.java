package com.school;

public class Person extends Resource {
	
	private final String firstName;
	private final String lastName;

	public Person(String id, String firstName, String lastName) {
		super(id);
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

}
