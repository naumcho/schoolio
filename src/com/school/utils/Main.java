package com.school.utils;

import com.school.School;
import com.school.SchoolSerializer;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length != 2) {
			System.err.println("Usage: schoolio <infile> <outfile>");
			System.exit(-1);
		}
		
		final String inFileName = args[0];
		final String outFileName = args[1];
		
		try {
			SchoolSerializer inSerializer = SchoolSerializer.makeSerializer(inFileName);
			School school = inSerializer.buildFromFile(inFileName);
			
			SchoolSerializer outSerializer = SchoolSerializer.makeSerializer(outFileName);
			outSerializer.writeToFile(school, outFileName);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-2);
		}

	}

}
