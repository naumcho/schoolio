package com.school;

import java.util.Map;

import com.google.common.collect.Maps;

public class Classroom extends Resource {
	private final String name;
	
	Map<String, Teacher> teachers = Maps.newLinkedHashMap();
	Map<String, Student> students = Maps.newLinkedHashMap();

	public Classroom(String id, String name) {
		super(id);
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "Classroom [name=" + name + ", teachers=" + teachers.toString()
				+ ", students=" + students.toString() + ", getId()=" + getId() + "]";
	}

	public void addTeacher(String teacher1Id, Teacher teacher1) {
		if (teachers.get(teacher1Id) == null) {
			teachers.put(teacher1Id, teacher1);
		}

		
	}

	public void addStudent(String studentId, Student student) {
		if (students.get(studentId) == null) {
			students.put(studentId, student);
		}

	}

}
