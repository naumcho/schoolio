package com.school;

import java.util.Map;

import com.google.common.collect.Maps;

public class Grade extends Resource {

	Map<String, Classroom> classrooms = Maps.newLinkedHashMap();
	
	public Grade(String id) {
		super(id);
	}

	@Override
	public String toString() {
		return "Grade [classrooms=" + classrooms + ", getId()=" + getId() + "]";
	}

	public void addClassroom(String roomId, Classroom room) {

		//also add a reference to the grade list of classrooms
		if (classrooms.get(roomId) == null) {
			classrooms.put(roomId, room);
		}
		
	}


}
