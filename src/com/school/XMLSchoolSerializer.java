package com.school;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


/**
 * XML implementation of the serializer
 * 
 * @author root
 *
 */
public class XMLSchoolSerializer extends SchoolSerializer {

	@Override
	public School buildFromFile(String inFileName) throws IOException {

		Document dom = readXML(inFileName);

		Element schoolEl = dom.getDocumentElement();

		School school = new School(schoolEl.getAttribute("id"), schoolEl.getAttribute("name"));
		ResourceStore resourceStore = new ResourceStore();

		NodeList grades = schoolEl.getElementsByTagName("grade");
		for (int i = 0; i < grades.getLength(); i++) {
			Element gradeEl = (Element) grades.item(i);
			String gradeId = gradeEl.getAttribute("id");

			Grade grade = resourceStore.grades.get(gradeId);
			if (grade == null) {
				grade = new Grade(gradeId);
				resourceStore.grades.put(gradeId, grade);
			}
			school.addGrade(gradeId,grade);
			
			NodeList classrooms = gradeEl.getElementsByTagName("classroom");
			for (int j = 0; j < classrooms.getLength(); j++) {
				Element roomEl = (Element) classrooms.item(j);
				String roomId = roomEl.getAttribute("id");

				Classroom room = resourceStore.classrooms.get(roomId);
				if (room == null) {
					String roomName = roomEl.getAttribute("name");
					room = new Classroom(roomId, roomName);
					resourceStore.classrooms.put(roomId, room);
				}
				grade.addClassroom(roomId, room);

				NodeList teachers = roomEl.getElementsByTagName("teacher");
				for (int t = 0; t < teachers.getLength(); t++) {
					Element teacherEl = (Element) teachers.item(t);
					String teacherId = teacherEl.getAttribute("id");

					Teacher teacher = resourceStore.teachers.get(teacherId);
					if (teacher == null) {
						String firstName = teacherEl.getAttribute("first_name");
						String lastName = teacherEl.getAttribute("last_name");
						teacher = new Teacher(teacherId, firstName, lastName);
						resourceStore.teachers.put(teacherId, teacher);
					}
					//also add a reference to the grade-classroom
					room.addTeacher(teacherId, teacher);
				}

				NodeList students = roomEl.getElementsByTagName("student");
				for (int t = 0; t < students.getLength(); t++) {
					Element studentEl = (Element) students.item(t);
					String studentId = studentEl.getAttribute("id");

					Student student = resourceStore.students.get(studentId);
					if (student == null) {
						String firstName = studentEl.getAttribute("first_name");
						String lastName = studentEl.getAttribute("last_name");
						student = new Student(studentId, firstName, lastName);
						resourceStore.students.put(studentId, student);
					}
					//also add a reference to the grade-classroom
					room.addStudent(studentId, student);
				}
			}

		}

		return school;
	}

	/**
	 * @param inFileName
	 * @return
	 * @throws IOException
	 */
	private Document readXML(String inFileName) throws IOException {
		Document dom = null;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();

			//parse using builder to get DOM representation of the XML file
			dom = db.parse(inFileName);
		} catch (SAXException e) {
			e.printStackTrace();
			throw new IOException(e);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			throw new IOException(e);
		}
		return dom;
	}

	@Override
	public void writeToFile(School school, String path) throws IOException {
		Document doc = buildXML(school); 

		saveFile(doc, path);
	}

	/**
	 * @param school
	 * @return
	 * @throws IOException
	 */
	Document buildXML(School school) throws IOException {
		Document doc = null;
		
		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			doc = docBuilder.newDocument();

			Element rootElement = doc.createElement("school");
			rootElement.setAttribute("id", school.getId());
			rootElement.setAttribute("name", school.getName());
			doc.appendChild(rootElement);

			for(Grade grade : school.grades.values()) {
				Element gradeElement = doc.createElement("grade");
				gradeElement.setAttribute("id", grade.getId());
				rootElement.appendChild(gradeElement);

				for(Classroom room : grade.classrooms.values()) {
					Element roomElement = doc.createElement("classroom");
					roomElement.setAttribute("id", room.getId());
					roomElement.setAttribute("name", room.getName());
					gradeElement.appendChild(roomElement);

					for(Teacher teacher : room.teachers.values()) {
						Element teacherElement = doc.createElement("teacher");
						teacherElement.setAttribute("id", teacher.getId());
						teacherElement.setAttribute("first_name", teacher.getFirstName());
						teacherElement.setAttribute("last_name", teacher.getLastName());
						roomElement.appendChild(teacherElement);
					}

					for(Student student : room.students.values()) {
						Element studentElement = doc.createElement("student");
						studentElement.setAttribute("id", student.getId());
						studentElement.setAttribute("first_name", student.getFirstName());
						studentElement.setAttribute("last_name", student.getLastName());
						roomElement.appendChild(studentElement);
					}
				}
			}

	
		} catch (ParserConfigurationException e) {
			throw new IOException(e);
		}
		return doc;
	}

	/** Save the DOM to file
	 * 
	 * @param doc
	 * @param path
	 * @throws TransformerFactoryConfigurationError
	 * @throws TransformerConfigurationException
	 * @throws TransformerException
	 */
	private void saveFile(Document doc, String path)
			throws IOException {
		try {
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(path));

			transformer.transform(source, result);

		} catch (TransformerConfigurationException e) {
			throw new IOException(e);
		} catch (TransformerException e) {
			throw new IOException(e);
		}
	}

	public static String domToString(Document doc) {
		String output = "";
		try {
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			output = writer.getBuffer().toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return output;
	}

}
